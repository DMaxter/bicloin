use super::frontend::Frontend;

use std::collections::HashMap;
use std::fs::read_to_string;
use std::sync::RwLock;

use regex::Regex;

#[derive(Debug)]
pub struct User {
    pub name: String,
    pub username: String,
    pub phone: String,
    pub lock: RwLock<()>,
}

impl User {
    /// Load stations from given file
    pub fn from_file(filename: &str) -> HashMap<String, Self> {
        let file = read_to_string(filename).expect("Can't open given users.csv file!");
        let mut users = HashMap::new();

        let name_regex: Regex = Regex::new(r"^\w{3,30}$").unwrap();
        let username_regex: Regex = Regex::new(r"^\w{3,10}$").unwrap();
        let phone_regex: Regex = Regex::new(r"\+\d{3,}$").unwrap();

        file.split("\n").for_each(|l| {
            let user: Vec<&str> = l.split(",").collect();

            if user.len() != 3
                && !name_regex.is_match(user[0])
                && !username_regex.is_match(user[1])
                && !phone_regex.is_match(user[2])
            {
                panic!("Invalid format in users.csv!");
            }

            users.insert(
                user[0].to_owned(),
                User {
                    username: user[0].to_owned(),
                    name: user[1].to_owned(),
                    phone: user[2].to_owned(),
                    lock: RwLock::new(()),
                },
            );
        });

        users
    }
}

#[derive(Debug)]
pub struct Coordinates(f64, f64);

impl Coordinates {
    /// Check if `latitude` is a valid latitude value
    pub fn check_latitude(latitude: f64) -> bool {
        latitude >= -90.0 && latitude <= 90.0
    }

    /// Check if `longitude` is a valid longitude value
    pub fn check_longitude(longitude: f64) -> bool {
        longitude >= -180.0 && longitude <= 180.0
    }

    /// Create valid Earth decimal coordinates
    pub fn new(latitude: f64, longitude: f64) -> Self {
        if !Coordinates::check_latitude(latitude) || !Coordinates::check_longitude(longitude) {
            panic!("Invalid coordinates!");
        }

        Coordinates(latitude, longitude)
    }

    /// Get latitude
    pub fn get_latitude(&self) -> f64 {
        self.0
    }

    /// Get longitude
    pub fn get_longitude(&self) -> f64 {
        self.1
    }
}

#[derive(Debug)]
pub struct Station {
    pub name: String,
    pub id: String,
    pub location: Coordinates,
    pub capacity: u32,
    pub prize: u32,
    pub lock: RwLock<()>,
}

impl Station {
    /// Load stations from given file
    pub async fn from_file(
        filename: &str,
        mut frontend: Option<&mut Frontend>,
    ) -> HashMap<String, Self> {
        let file = read_to_string(filename).expect("Can't open given stations.csv file!");
        let mut stations = HashMap::new();

        let init: bool = frontend.is_some();

        let id_regex: Regex = Regex::new(r"^\w{4}$").unwrap();

        for l in file.split("\n") {
            let station: Vec<&str> = l.split(",").collect();

            if station.len() != 7 && !id_regex.is_match(station[1]) {
                panic!("Invalid format in stations.csv!");
            }

            stations.insert(
                station[1].to_owned(),
                Station {
                    name: station[0].to_owned(),
                    id: station[1].to_owned(),
                    location: Coordinates::new(
                        station[2]
                            .parse::<f64>()
                            .expect("Latitude must be a number!"),
                        station[3]
                            .parse::<f64>()
                            .expect("Longitude must be a number!"),
                    ),
                    capacity: station[4]
                        .parse::<u32>()
                        .expect("Capacity must be an integer!"),
                    prize: station[6]
                        .parse::<u32>()
                        .expect("Prize must be an integer!"),
                    lock: RwLock::new(()),
                },
            );

            if init {
                match frontend
                    .as_mut()
                    .unwrap()
                    .write_number_bikes(
                        station[1],
                        station[5]
                            .parse::<u32>()
                            .expect("Number of bikes must be an integer!"),
                    )
                    .await
                {
                    Ok(_) => (),
                    Err(e) => panic!("An error occurred while initializing stations\n{:#?}", e),
                };
            }
        }

        stations
    }
}
