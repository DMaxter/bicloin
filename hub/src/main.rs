use hub::grpc::hub_server::HubServer;
use hub::server_impl::MyHub;

use hub::frontend::Frontend;

use std::env::args;
use std::net::ToSocketAddrs;
use std::process::exit;

use tonic::transport::Server;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = args().collect();

    let num_args = args.len();
    if num_args < 8 || num_args > 9 {
        println!("Wrong number of arguments!");
        println!(
            "Usage: hub <zookeeper host> <zookeeper port> <hub host> <hub port> <hub num> <users.csv> <stations.csv> [initRec]"
        );

        exit(1);
    }

    let zoo_host = &args[1];
    let zoo_port = &args[2].parse::<u16>().expect("Invalid ZooKeeper port!");
    let hub_host = &args[3];
    let hub_port = &args[4].parse::<u16>().expect("Invalid Hub port!");
    let hub_num = &args[5].parse::<usize>().expect("Invalid Hub number!");
    let users = &args[6];
    let stations = &args[7];
    let init_rec = if num_args == 9 {
        &args[8] == "initRec"
    } else {
        false
    };

    // Build address
    let mut addr_str = String::from(hub_host);
    addr_str.push(':');
    addr_str.push_str(&hub_port.to_string());

    let addr = addr_str
        .to_socket_addrs()
        .expect("Couldn't resolve domain!")
        .next()
        .unwrap();

    let frontend = Frontend::new("://[::1]:8090")
        .await
        .expect("Cannot connect to rec!");

    println!("Listening on {}", addr);

    Server::builder()
        .add_service(HubServer::new(
            MyHub::new(users, stations, init_rec, frontend).await,
        ))
        .serve(addr)
        .await?;

    Ok(())
}
