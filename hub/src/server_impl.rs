use super::domain::*;
use super::frontend::Frontend;
use super::grpc::hub_server::Hub;
use super::grpc::Coordinates as GrpcCoords;
use super::grpc::{
    BalanceRequest, BalanceResponse, BikeDownRequest, BikeDownResponse, BikeUpRequest,
    BikeUpResponse, InfoStationRequest, InfoStationResponse, LocateStationRequest,
    LocateStationResponse, PingRequest, PingResponse, SysStatusRequest, SysStatusResponse,
    TopUpRequest, TopUpResponse,
};

use std::collections::HashMap;

use tonic::{Code, Request, Response, Status};

enum ErrorMessage {
    EmptyStation,
    InsufficientFunds,
    InvalidAmount,
    InvalidCoordinates,
    InvalidNumStations,
    InvalidStation,
    InvalidUser,
    NotMatchingPhone,
    StationFull,
    StationTooFar,
    UserHasBike,
    UserHasNoBike,
}

impl ErrorMessage {
    pub fn get_message(self) -> &'static str {
        match self {
            ErrorMessage::EmptyStation => "Station doesn't have any available bikes",
            ErrorMessage::InsufficientFunds => "Not enough BIC to pickup bike",
            ErrorMessage::InvalidAmount => "Amount must be between 1 and 20 €",
            ErrorMessage::InvalidCoordinates => "Location is not valid",
            ErrorMessage::InvalidNumStations => "Number of stations must be a positive number",
            ErrorMessage::InvalidStation => "Station doesn't exist",
            ErrorMessage::InvalidUser => "User doesn't exist",
            ErrorMessage::NotMatchingPhone => "Phone number doesn't match",
            ErrorMessage::StationFull => "Station has not empty docks",
            ErrorMessage::StationTooFar => "Station is too far",
            ErrorMessage::UserHasBike => "User has already picked up a bike",
            ErrorMessage::UserHasNoBike => "User doesn't have a bike",
        }
    }
}

const MIN_STATION_DISTANCE: u32 = 200;
const BIKE_COST: u32 = 10;

pub struct MyHub {
    frontend: Frontend,
    users: HashMap<String, User>,
    stations: HashMap<String, Station>,
}

impl MyHub {
    pub async fn new(users: &str, stations: &str, init: bool, mut frontend: Frontend) -> Self {
        let stations =
            Station::from_file(stations, if init { Some(&mut frontend) } else { None }).await;

        MyHub {
            frontend,
            users: User::from_file(users),
            stations,
        }
    }
}

#[tonic::async_trait]
impl Hub for MyHub {
    async fn balance(
        &self,
        request: Request<BalanceRequest>,
    ) -> Result<Response<BalanceResponse>, Status> {
        let user = &request.get_ref().user;

        if !self.users.contains_key(user) {
            return Err(Status::new(
                Code::InvalidArgument,
                ErrorMessage::get_message(ErrorMessage::InvalidUser),
            ));
        }

        match self.frontend.get_balance(user).await {
            Ok(val) => Ok(Response::new(BalanceResponse { amount: val })),
            Err(_) => todo!(),
        }
    }

    async fn top_up(
        &self,
        request: Request<TopUpRequest>,
    ) -> Result<Response<TopUpResponse>, Status> {
        let user = &request.get_ref().user;
        let amount = &request.get_ref().amount;
        let phone = &request.get_ref().phone;

        if !self.users.contains_key(user) {
            return Err(Status::new(
                Code::InvalidArgument,
                ErrorMessage::InvalidUser.get_message(),
            ));
        }

        if *amount > 20 || *amount < 1 {
            return Err(Status::new(
                Code::InvalidArgument,
                ErrorMessage::InvalidAmount.get_message(),
            ));
        }

        let user_record = self.users.get(user).unwrap();
        if user_record.phone != *phone {
            return Err(Status::new(
                Code::FailedPrecondition,
                ErrorMessage::NotMatchingPhone.get_message(),
            ));
        }

        match self.frontend.top_up(user, *amount * 10).await {
            Ok(val) => Ok(Response::new(TopUpResponse { amount: val })),
            Err(_) => todo!(),
        }
    }

    async fn info_station(
        &self,
        request: Request<InfoStationRequest>,
    ) -> Result<Response<InfoStationResponse>, Status> {
        let station = &request.get_ref().station;

        if !self.stations.contains_key(station) {
            return Err(Status::new(
                Code::InvalidArgument,
                ErrorMessage::InvalidStation.get_message(),
            ));
        }

        let (pickups, deliveries) = match self.frontend.get_stats(station).await {
            Ok(val) => val,
            Err(_) => todo!(),
        };

        let available = match self.frontend.get_available(station).await {
            Ok(val) => val,
            Err(_) => todo!(),
        };

        let record = self.stations.get(station).unwrap();

        Ok(Response::new(InfoStationResponse {
            name: record.name.to_owned(),
            location: Some(GrpcCoords {
                latitude: record.location.get_latitude(),
                longitude: record.location.get_longitude(),
            }),
            capacity: record.capacity,
            prize: record.prize,
            available,
            pickups,
            deliveries,
        }))
    }

    async fn locate_station(
        &self,
        request: Request<LocateStationRequest>,
    ) -> Result<Response<LocateStationResponse>, Status> {
        let coords = match &request.get_ref().location {
            Some(val) => val,
            None => {
                return Err(Status::new(
                    Code::InvalidArgument,
                    ErrorMessage::InvalidCoordinates.get_message(),
                ))
            }
        };

        let user_lat = coords.latitude.to_radians();
        let user_lon = coords.longitude.to_radians();

        let num = &request.get_ref().num_stations;

        if *num <= 0 {
            return Err(Status::new(
                Code::InvalidArgument,
                ErrorMessage::InvalidNumStations.get_message(),
            ));
        }

        let mut dist_vec: Vec<(String, u32)> = self
            .stations
            .iter()
            .map(|(name, station)| {
                (
                    name.clone(),
                    haversine_distance(
                        station.location.get_latitude().to_radians(),
                        station.location.get_longitude().to_radians(),
                        user_lat,
                        user_lon,
                    ),
                )
            })
            .collect();

        dist_vec.sort_by(|a, b| a.1.cmp(&b.1));

        Ok(Response::new(LocateStationResponse {
            stations: dist_vec
                .iter()
                .map(|(name, _)| name.clone())
                .take(*num as usize)
                .collect::<Vec<String>>(),
        }))
    }

    async fn bike_up(
        &self,
        request: Request<BikeUpRequest>,
    ) -> Result<Response<BikeUpResponse>, Status> {
        let username = &request.get_ref().user;
        let station_name = &request.get_ref().station;
        let coords = match &request.get_ref().location {
            Some(val) => val,
            None => {
                return Err(Status::new(
                    Code::InvalidArgument,
                    ErrorMessage::InvalidCoordinates.get_message(),
                ))
            }
        };

        if !self.users.contains_key(username) {
            return Err(Status::new(
                Code::InvalidArgument,
                ErrorMessage::get_message(ErrorMessage::InvalidUser),
            ));
        }

        let user = self.users.get(username).unwrap();

        if !self.stations.contains_key(station_name) {
            return Err(Status::new(
                Code::InvalidArgument,
                ErrorMessage::InvalidStation.get_message(),
            ));
        }

        let station = self.stations.get(station_name).unwrap();

        if haversine_distance(
            coords.latitude.to_radians(),
            coords.longitude.to_radians(),
            station.location.get_latitude().to_radians(),
            station.location.get_longitude().to_radians(),
        ) > MIN_STATION_DISTANCE
        {
            return Err(Status::new(
                Code::OutOfRange,
                ErrorMessage::StationTooFar.get_message(),
            ));
        }

        if match self.frontend.has_bike(username).await {
            Ok(val) => val,
            Err(_) => todo!(),
        } {
            return Err(Status::new(
                Code::FailedPrecondition,
                ErrorMessage::UserHasBike.get_message(),
            ));
        }

        let balance = match self.frontend.get_balance(username).await {
            Ok(val) => val,
            Err(_) => todo!(),
        };

        if balance < BIKE_COST {
            return Err(Status::new(
                Code::FailedPrecondition,
                ErrorMessage::InsufficientFunds.get_message(),
            ));
        }

        let bikes = match self.frontend.get_available(station_name).await {
            Ok(val) => val,
            Err(_) => todo!(),
        };

        if bikes == 0 {
            return Err(Status::new(
                Code::FailedPrecondition,
                ErrorMessage::EmptyStation.get_message(),
            ));
        }

        match self
            .frontend
            .write_number_bikes(station_name, bikes - 1)
            .await
        {
            Ok(_) => (),
            Err(_) => todo!(),
        };

        match self
            .frontend
            .set_balance(&username, balance - BIKE_COST)
            .await
        {
            Ok(_) => (),
            Err(_) => todo!(),
        };

        match self.frontend.set_bike(&username, true).await {
            Ok(_) => (),
            Err(_) => todo!(),
        };

        match self.frontend.inc_pickups(station_name).await {
            Ok(_) => (),
            Err(_) => todo!(),
        };

        Ok(Response::new(BikeUpResponse {}))
    }

    async fn bike_down(
        &self,
        request: Request<BikeDownRequest>,
    ) -> Result<Response<BikeDownResponse>, Status> {
        let username = &request.get_ref().user;
        let station_name = &request.get_ref().station;
        let coords = match &request.get_ref().location {
            Some(val) => val,
            None => {
                return Err(Status::new(
                    Code::InvalidArgument,
                    ErrorMessage::InvalidCoordinates.get_message(),
                ))
            }
        };

        if !self.users.contains_key(username) {
            return Err(Status::new(
                Code::InvalidArgument,
                ErrorMessage::get_message(ErrorMessage::InvalidUser),
            ));
        }

        let user = self.users.get(username).unwrap();

        if !self.stations.contains_key(station_name) {
            return Err(Status::new(
                Code::InvalidArgument,
                ErrorMessage::InvalidStation.get_message(),
            ));
        }

        let station = self.stations.get(station_name).unwrap();

        if haversine_distance(
            coords.latitude.to_radians(),
            coords.longitude.to_radians(),
            station.location.get_latitude().to_radians(),
            station.location.get_longitude().to_radians(),
        ) > MIN_STATION_DISTANCE
        {
            return Err(Status::new(
                Code::OutOfRange,
                ErrorMessage::StationTooFar.get_message(),
            ));
        }

        if !match self.frontend.has_bike(username).await {
            Ok(val) => val,
            Err(_) => todo!(),
        } {
            return Err(Status::new(
                Code::FailedPrecondition,
                ErrorMessage::UserHasNoBike.get_message(),
            ));
        }

        let bikes = match self.frontend.get_available(station_name).await {
            Ok(val) => val,
            Err(_) => todo!(),
        };

        if bikes == station.capacity {
            return Err(Status::new(
                Code::FailedPrecondition,
                ErrorMessage::StationFull.get_message(),
            ));
        }

        match self
            .frontend
            .write_number_bikes(station_name, bikes + 1)
            .await
        {
            Ok(_) => (),
            Err(_) => todo!(),
        };

        match self.frontend.set_bike(&username, false).await {
            Ok(_) => (),
            Err(_) => todo!(),
        };

        match self.frontend.inc_deliveries(station_name).await {
            Ok(_) => (),
            Err(_) => todo!(),
        };

        Ok(Response::new(BikeDownResponse {}))
    }

    async fn ping(&self, request: Request<PingRequest>) -> Result<Response<PingResponse>, Status> {
        match self.frontend.ping(&request.into_inner().message).await {
            Ok(val) => Ok(Response::new(PingResponse { message: val })),
            Err(_) => todo!(),
        }
    }

    async fn sys_status(
        &self,
        request: Request<SysStatusRequest>,
    ) -> Result<Response<SysStatusResponse>, Status> {
        todo!()
    }
}

const EARTH_RADIUS: f64 = 6_367_444.5;

fn haversine_distance(lat1: f64, lon1: f64, lat2: f64, lon2: f64) -> u32 {
    (2. * EARTH_RADIUS
        * (((lat2 - lat1) / 2.).sin().powi(2)
            + lat1.cos() * lat2.cos() * ((lon2 - lon1) / 2.0).sin().powi(2))
        .sqrt()
        .asin()) as u32
}
