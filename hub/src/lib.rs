pub mod server_impl;
pub mod grpc;
pub mod frontend;

mod domain;

#[derive(Debug)]
pub enum Error {
    GRPC,
    Type,
}
