use super::Error;

use rec::grpc::rec_client::RecClient;
use rec::grpc::value::Value as ValueEnum;
use rec::grpc::Value;
use rec::grpc::{PingRequest, ReadRequest, WriteRequest};

use tonic::transport::{Channel, Uri};
use tonic::Request;

const STATION_PREFIX: &str = "station";
const STATION_BIKENUM_SUFFIX: &str = "bikes";
const STATION_PICKUPS_SUFFIX: &str = "pickups";
const STATION_DELIVERIES_SUFFIX: &str = "deliveries";

const USER_PREFIX: &str = "user";
const USER_BIKE_SUFFIX: &str = "bike";
const USER_BALANCE_SUFFIX: &str = "balance";

pub struct Frontend {
    channel: Channel,
}

impl Frontend {
    pub async fn new(addr: &str) -> Option<Self> {
        let channel = match Channel::builder(addr.parse::<Uri>().unwrap())
            .connect()
            .await
        {
            Ok(val) => val,
            Err(e) => {
                println!("Couldn't connect to rec!\n{}", e);

                return None;
            }
        };

        Some(Frontend { channel })
    }

    pub async fn write_number_bikes(&self, name: &str, num: u32) -> Result<(), Error> {
        let response = match RecClient::new(self.channel.clone())
            .write(Request::new(WriteRequest {
                name: format!("{}_{}_{}", STATION_PREFIX, name, STATION_BIKENUM_SUFFIX),
                value: Some(Value {
                    value: Some(ValueEnum::Num(num)),
                }),
            }))
            .await
        {
            Ok(val) => val.into_inner(),
            Err(e) => {
                println!("ERR {}", e);

                return Err(Error::GRPC);
            }
        };

        Ok(())
    }

    pub async fn get_balance(&self, name: &str) -> Result<u32, Error> {
        let response = match RecClient::new(self.channel.clone())
            .read(Request::new(ReadRequest {
                name: format!("{}_{}_{}", USER_PREFIX, name, USER_BALANCE_SUFFIX),
            }))
            .await
        {
            Ok(val) => val.into_inner(),
            Err(e) => {
                println!("ERR {}", e);

                return Err(Error::GRPC);
            }
        };

        match response.value {
            Some(val) => match val.value {
                None => Ok(0),
                Some(ValueEnum::Num(num)) => Ok(num),
                Some(ValueEnum::Boolean(_)) => Err(Error::Type),
            },
            None => Ok(0),
        }
    }

    pub async fn top_up(&self, name: &str, amount: u32) -> Result<u32, Error> {
        let mut client = RecClient::new(self.channel.clone());
        let record = format!("{}_{}_{}", USER_PREFIX, name, USER_BALANCE_SUFFIX);

        // Get current balance
        let current = match client
            .read(Request::new(ReadRequest {
                name: record.clone(),
            }))
            .await
        {
            Ok(val) => val.into_inner(),
            Err(e) => {
                println!("ERR {}", e);

                return Err(Error::GRPC);
            }
        };

        let total = match current.value {
            Some(val) => match val.value {
                None => 0,
                Some(ValueEnum::Num(num)) => num,
                Some(ValueEnum::Boolean(_)) => return Err(Error::Type),
            },
            None => 0,
        } + amount;

        // Write new balance
        match client
            .write(Request::new(WriteRequest {
                name: record,
                value: Some(Value {
                    value: Some(ValueEnum::Num(total)),
                }),
            }))
            .await
        {
            Ok(_) => Ok(total),
            Err(e) => {
                println!("ERR {}", e);

                Err(Error::GRPC)
            }
        }
    }

    pub async fn get_stats(&self, station: &str) -> Result<(u32, u32), Error> {
        let mut client = RecClient::new(self.channel.clone());
        let pickups_record = format!("{}_{}_{}", STATION_PREFIX, station, STATION_PICKUPS_SUFFIX);
        let deliveries_record = format!(
            "{}_{}_{}",
            STATION_PREFIX, station, STATION_DELIVERIES_SUFFIX
        );

        let pickups = match client
            .read(Request::new(ReadRequest {
                name: pickups_record,
            }))
            .await
        {
            Ok(val) => match val.into_inner().value {
                Some(val) => match val.value {
                    None => 0,
                    Some(ValueEnum::Num(num)) => num,
                    Some(ValueEnum::Boolean(_)) => return Err(Error::Type),
                },
                None => 0,
            },

            Err(e) => {
                println!("ERR {}", e);

                return Err(Error::GRPC);
            }
        };

        let deliveries = match client
            .read(Request::new(ReadRequest {
                name: deliveries_record,
            }))
            .await
        {
            Ok(val) => match val.into_inner().value {
                Some(val) => match val.value {
                    None => 0,
                    Some(ValueEnum::Num(num)) => num,
                    Some(ValueEnum::Boolean(_)) => return Err(Error::Type),
                },
                None => 0,
            },

            Err(e) => {
                println!("ERR {}", e);

                return Err(Error::GRPC);
            }
        };

        Ok((pickups, deliveries))
    }

    pub async fn get_available(&self, station: &str) -> Result<u32, Error> {
        match RecClient::new(self.channel.clone())
            .read(Request::new(ReadRequest {
                name: format!("{}_{}_{}", STATION_PREFIX, station, STATION_BIKENUM_SUFFIX),
            }))
            .await
        {
            Ok(val) => match val.into_inner().value {
                Some(val) => match val.value {
                    None => Ok(0),
                    Some(ValueEnum::Num(num)) => Ok(num),
                    Some(ValueEnum::Boolean(_)) => return Err(Error::Type),
                },
                None => Ok(0),
            },
            Err(e) => {
                println!("ERR {}", e);

                Err(Error::GRPC)
            }
        }
    }

    pub async fn has_bike(&self, user: &str) -> Result<bool, Error> {
        match RecClient::new(self.channel.clone())
            .read(Request::new(ReadRequest {
                name: format!("{}_{}_{}", USER_PREFIX, user, USER_BIKE_SUFFIX),
            }))
            .await
        {
            Ok(val) => match val.into_inner().value {
                Some(val) => match val.value {
                    None => Ok(false),
                    Some(ValueEnum::Boolean(val)) => Ok(val),
                    Some(ValueEnum::Num(_)) => return Err(Error::Type),
                },
                None => Ok(false),
            },
            Err(e) => {
                println!("ERR {}", e);

                Err(Error::GRPC)
            }
        }
    }

    pub async fn set_bike(&self, user: &str, has_bike: bool) -> Result<(), Error> {
        match RecClient::new(self.channel.clone())
            .write(Request::new(WriteRequest {
                name: format!("{}_{}_{}", USER_PREFIX, user, USER_BIKE_SUFFIX),
                value: Some(Value {
                    value: Some(ValueEnum::Boolean(has_bike)),
                }),
            }))
            .await
        {
            Ok(_) => Ok(()),
            Err(e) => {
                println!("ERR {}", e);

                Err(Error::GRPC)
            }
        }
    }

    pub async fn set_balance(&self, user: &str, amount: u32) -> Result<(), Error> {
        match RecClient::new(self.channel.clone())
            .write(Request::new(WriteRequest {
                name: format!("{}_{}_{}", USER_PREFIX, user, USER_BALANCE_SUFFIX),
                value: Some(Value {
                    value: Some(ValueEnum::Num(amount)),
                }),
            }))
            .await
        {
            Ok(_) => Ok(()),
            Err(e) => {
                println!("ERR {}", e);

                Err(Error::GRPC)
            }
        }
    }

    pub async fn inc_deliveries(&self, station: &str) -> Result<(), Error> {
        let mut client = RecClient::new(self.channel.clone());
        let record = format!(
            "{}_{}_{}",
            STATION_PREFIX, station, STATION_DELIVERIES_SUFFIX
        );

        let deliveries = match client
            .read(Request::new(ReadRequest {
                name: record.clone(),
            }))
            .await
        {
            Ok(val) => match val.into_inner().value {
                Some(val) => match val.value {
                    None => 0,
                    Some(ValueEnum::Num(val)) => val,
                    Some(ValueEnum::Boolean(_)) => return Err(Error::Type),
                },
                None => 0,
            },
            Err(e) => {
                println!("ERR {}", e);

                return Err(Error::GRPC);
            }
        };

        match client
            .write(Request::new(WriteRequest {
                name: record,
                value: Some(Value {
                    value: Some(ValueEnum::Num(deliveries + 1)),
                }),
            }))
            .await
        {
            Ok(_) => Ok(()),
            Err(e) => {
                println!("ERR {}", e);

                Err(Error::GRPC)
            }
        }
    }

    pub async fn inc_pickups(&self, station: &str) -> Result<(), Error> {
        let mut client = RecClient::new(self.channel.clone());
        let record = format!("{}_{}_{}", STATION_PREFIX, station, STATION_PICKUPS_SUFFIX);

        let pickups = match client
            .read(Request::new(ReadRequest {
                name: record.clone(),
            }))
            .await
        {
            Ok(val) => match val.into_inner().value {
                Some(val) => match val.value {
                    None => 0,
                    Some(ValueEnum::Num(val)) => val,
                    Some(ValueEnum::Boolean(_)) => return Err(Error::Type),
                },
                None => 0,
            },
            Err(e) => {
                println!("ERR {}", e);

                return Err(Error::GRPC);
            }
        };

        match client
            .write(Request::new(WriteRequest {
                name: record,
                value: Some(Value {
                    value: Some(ValueEnum::Num(pickups + 1)),
                }),
            }))
            .await
        {
            Ok(_) => Ok(()),
            Err(e) => {
                println!("ERR {}", e);

                Err(Error::GRPC)
            }
        }
    }

    pub async fn ping(&self, message: &str) -> Result<String, Error> {
        match RecClient::new(self.channel.clone())
            .ping(Request::new(PingRequest {
                message: message.to_owned(),
            }))
            .await
        {
            Ok(val) => Ok(val.into_inner().message),
            Err(e) => {
                println!("ERR {}", e);

                Err(Error::GRPC)
            }
        }
    }
}
