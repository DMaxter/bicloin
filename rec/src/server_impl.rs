use super::domain::RecordType;
use super::grpc::rec_server::Rec;
use super::grpc::value::Value as ValueEnum;
use super::grpc::{
    PingRequest, PingResponse, ReadRequest, ReadResponse, Value, WriteRequest, WriteResponse,
};

use std::collections::HashMap;
use std::sync::RwLock;

use tonic::{Code, Request, Response, Status};

enum Error {
    WriteLock,
    ReadLock,
    EmptyVal,
}

impl Error {
    pub fn get_message(self) -> &'static str {
        match self {
            Error::WriteLock => "Couldn't acquire write lock",
            Error::ReadLock => "Couldn't acquire read lock",
            Error::EmptyVal => "Value cannot be empty",
        }
    }
}

#[derive(Default)]
pub struct MyRec {
    records: RwLock<HashMap<String, RecordType>>,
}

impl MyRec {
    pub fn new() -> Self {
        MyRec {
            records: RwLock::new(HashMap::new()),
        }
    }
}

#[tonic::async_trait]
impl Rec for MyRec {
    async fn read(&self, request: Request<ReadRequest>) -> Result<Response<ReadResponse>, Status> {
        let id = &request.get_ref().name;

        let val = match self.records.read() {
            Ok(lock) => match lock.get(id) {
                None => None,
                Some(RecordType::None) => Some(Value { value: None }),
                Some(RecordType::Integer(val)) => Some(Value {
                    value: Some(ValueEnum::Num(*val)),
                }),
                Some(RecordType::Boolean(val)) => Some(Value {
                    value: Some(ValueEnum::Boolean(*val)),
                }),
            },
            Err(_) => {
                return Err(Status::new(
                    Code::Unavailable,
                    Error::get_message(Error::ReadLock),
                ))
            }
        };

        // Check if needs to initialize new register
        match val {
            None => {
                // Acquire write lock
                match self.records.write() {
                    Ok(mut lock) => {
                        lock.insert(id.to_string(), RecordType::None);
                        ()
                    }
                    Err(_) => {
                        return Err(Status::new(
                            Code::Unavailable,
                            Error::get_message(Error::WriteLock),
                        ))
                    }
                }
            }
            Some(_) => (),
        }

        Ok(Response::new(ReadResponse {
            // Acquire read lock
            value: val,
        }))
    }

    async fn write(
        &self,
        request: Request<WriteRequest>,
    ) -> Result<Response<WriteResponse>, Status> {
        let id = &request.get_ref().name;
        let value = match &request.get_ref().value {
            None => {
                return Err(Status::new(
                    Code::InvalidArgument,
                    Error::get_message(Error::EmptyVal),
                ))
            }
            Some(val) => match val.value {
                None => {
                    return Err(Status::new(
                        Code::InvalidArgument,
                        Error::get_message(Error::EmptyVal),
                    ))
                }
                Some(ValueEnum::Boolean(boolean)) => RecordType::Boolean(boolean),
                Some(ValueEnum::Num(num)) => RecordType::Integer(num),
            },
        };

        match self.records.write() {
            Ok(mut lock) => {
                lock.insert(id.to_owned(), value);
                Ok(Response::new(WriteResponse {}))
            }
            Err(_) => Err(Status::new(
                Code::Unavailable,
                Error::get_message(Error::WriteLock),
            )),
        }
    }

    async fn ping(&self, request: Request<PingRequest>) -> Result<Response<PingResponse>, Status> {
        Ok(Response::new(PingResponse {
            message: request.into_inner().message,
        }))
    }
}
