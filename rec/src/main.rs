use rec::grpc::rec_server::RecServer;
use rec::server_impl::MyRec;

use tonic::transport::Server;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let addr = "[::1]:8090".parse().unwrap();

    println!("Listening on {}", addr);

    Server::builder()
        .add_service(RecServer::new(MyRec::new()))
        .serve(addr)
        .await?;

    Ok(())
}
