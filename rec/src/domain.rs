#[derive(Debug)]
pub enum RecordType {
    Boolean(bool),
    Integer(u32),
    None,
}
