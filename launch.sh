#!/usr/bin/env bash

if [ $# -ne 1 ]; then
    NUM=3
fi

if ! nc -z localhost 2181; then
    echo "ZooKeeper not running"
    #exit
fi

if [ -f .env ] ; then
  export $(cat .env | sed 's/#.*//g' | xargs)
fi

SESSION=bicloin
EXISTS=$(tmux list-sessions | grep $SESSION)
if [ "$EXISTS" != "" ]; then
    tmux kill-session -t $SESSION
fi

tmux new-session -d -s $SESSION 

tmux rename-window -t 0 "Rec"

# Launch recs
tmux send-keys "cargo run --bin rec -- ${ZOO_HOST} ${ZOO_PORT} ${REC_HOST_1} ${REC_PORT_1} ${REC_INUM_1}" "C-m"
#tmux split-window -v 
#tmux send-keys "cargo run --bin rec -- ${ZOO_HOST} ${ZOO_PORT} ${REC_HOST_2} ${REC_PORT_2} ${REC_INUM_2}" "C-m"
#tmux split-window -h
#tmux send-keys "cargo run --bin rec -- ${ZOO_HOST} ${ZOO_PORT} ${REC_HOST_3} ${REC_PORT_3} ${REC_INUM_3}" "C-m"

if [ $NUM -ge 4 ]; then
    tmux split-window -h
    tmux send-keys "cargo run --bin rec -- ${ZOO_HOST} ${ZOO_PORT} ${REC_HOST_4} ${REC_PORT_4} ${REC_INUM_4}" "C-m"
fi

if [ $NUM -ge 5 ]; then
    tmux split-window -h 
    tmux send-keys "cargo run --bin rec -- ${ZOO_HOST} ${ZOO_PORT} ${REC_HOST_5} ${REC_PORT_5} ${REC_INUM_5}" "C-m"
fi

tmux select-layout main-vertical

# Create new window
tmux new-window -t $SESSION:1 -n "Hub/App"
tmux send-keys "sleep 1 && cargo run --bin hub -- ${ZOO_HOST} ${ZOO_PORT} ${HUB_HOST} ${HUB_PORT} ${HUB_INUM} ${HUB_DATA_DIR}/users.csv ${HUB_DATA_DIR}/stations.csv initRec" "C-m"
tmux split-window -v
tmux send-keys "sleep 2 && cargo run --bin app -- ${ZOO_HOST} ${ZOO_PORT} fabio +19074891438 39.7500 -8.8120"  "C-m"
    
tmux attach-session -t $SESSION:0
