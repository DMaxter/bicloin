use std::collections::HashMap;
use std::env::args;
use std::io::{stdin, Result};
use std::process::exit;

use app::commands::*;
use app::frontend::Frontend;
use app::Coordinates;
use app::Error;

#[tokio::main]
async fn main() -> Result<()> {
    let args: Vec<String> = args().collect();

    if args.len() != 7 {
        println!("Wrong number of arguments!");
        println!(
            "Usage: app <zookeeper host> <zookeeper port> <user> <phone> <latitude> <longitude>"
        );

        exit(1);
    }

    let zoo_host = &args[1];
    let zoo_port: u16 = args[2].parse::<u16>().expect("Invalid ZooKeeper port!");
    let user = &args[3];
    let phone = &args[4];
    let latitude: f64 = args[5].parse::<f64>().expect("Invalid latitude!");
    let longitude: f64 = args[6].parse::<f64>().expect("Invalid longitude!");
    let mut coordinates: Coordinates;

    let mut locations: HashMap<String, Coordinates> = HashMap::new();

    if let Some(coords) = Coordinates::new(latitude, longitude) {
        coordinates = coords;
    } else {
        println!("Latitude and/or longitude are not inside their expected values!");
        exit(-1);
    }

    println!("Bicloin App");

    let frontend = Frontend::new("://[::1]:8081").await.unwrap();

    loop {
        let mut input = String::new();
        stdin().read_line(&mut input)?;

        let split: Vec<&str> = input.trim().split(" ").collect();

        let command: Option<Box<dyn Command>> = match split[0] {
            "move" => Some(Box::new(Move::new(
                split[1..].to_vec(),
                &mut coordinates,
                user,
                &locations,
            ))),
            "at" => Some(Box::new(At::new(split[1..].to_vec(), &coordinates, user))),
            "balance" => Some(Box::new(Balance::new(split[1..].to_vec(), user, &frontend))),
            "top-up" => Some(Box::new(TopUp::new(
                split[1..].to_vec(),
                user,
                phone,
                &frontend,
            ))),
            "info" => Some(Box::new(Info::new(split[1..].to_vec(), &frontend))),
            "scan" => Some(Box::new(Scan::new(
                split[1..].to_vec(),
                &coordinates,
                &frontend,
            ))),
            "bike-up" => Some(Box::new(BikeUp::new(
                split[1..].to_vec(),
                user,
                &coordinates,
                &frontend,
            ))),
            "bike-down" => Some(Box::new(BikeDown::new(
                split[1..].to_vec(),
                user,
                &coordinates,
                &frontend,
            ))),
            "tag" => Some(Box::new(Tag::new(split[1..].to_vec(), &mut locations))),
            "ping" => Some(Box::new(Ping::new(split[1..].to_vec(), &frontend))),
            _ => None,
        };

        if let Some(mut com) = command {
            match com.execute().await {
                Ok(_) => println!("OK"),
                Err(e) if e == Error::Coord => println!("ERR Invalid coordinates"),
                Err(e) if e == Error::Usage => {
                    println!("ERR Wrong usage! Type \"help\" to see commands")
                }
                _ => unimplemented!(),
            }
        } else {
            println!("ERR Unknown command")
        }
    }
}
