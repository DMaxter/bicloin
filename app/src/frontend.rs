use super::{Coordinates, Error};

use hub::grpc::hub_client::HubClient;
use hub::grpc::Coordinates as GrpcCoordinates;
use hub::grpc::{
    BalanceRequest, BikeDownRequest, BikeUpRequest, InfoStationRequest, LocateStationRequest,
    PingRequest, TopUpRequest,
};

use tonic::transport::{Channel, Uri};
use tonic::Request;

pub struct StationInfo {
    pub name: String,
    pub location: Coordinates,
    pub capacity: u32,
    pub prize: u32,
    pub available: u32,
    pub pickups: u32,
    pub deliveries: u32,
}

pub struct Frontend {
    channel: Channel,
}

impl Frontend {
    pub async fn new(addr: &str) -> Option<Self> {
        let channel = match Channel::builder(addr.parse::<Uri>().unwrap())
            .connect()
            .await
        {
            Ok(val) => val,
            Err(e) => {
                println!("Couldn't connect to hub!\n{}", e);

                return None;
            }
        };

        Some(Frontend { channel })
    }

    pub async fn balance(&self, user: &str) -> Result<u32, Error> {
        match HubClient::new(self.channel.clone())
            .balance(Request::new(BalanceRequest {
                user: user.to_owned(),
            }))
            .await
        {
            Ok(val) => Ok(val.into_inner().amount),
            Err(e) => todo!("{}", e),
        }
    }

    pub async fn top_up(&self, user: &str, phone: &str, amount: u32) -> Result<u32, Error> {
        match HubClient::new(self.channel.clone())
            .top_up(Request::new(TopUpRequest {
                user: user.to_owned(),
                phone: phone.to_owned(),
                amount,
            }))
            .await
        {
            Ok(val) => Ok(val.into_inner().amount),
            Err(e) => todo!("{}", e),
        }
    }

    pub async fn info_station(&self, station: &str) -> Result<StationInfo, Error> {
        let response = match HubClient::new(self.channel.clone())
            .info_station(Request::new(InfoStationRequest {
                station: station.to_owned(),
            }))
            .await
        {
            Ok(val) => val.into_inner(),
            Err(_) => todo!(),
        };

        let coords = response.location.unwrap();

        Ok(StationInfo {
            name: response.name.to_owned(),
            location: match Coordinates::new(coords.latitude, coords.longitude) {
                Some(val) => val,
                None => return Err(Error::Coord),
            },
            capacity: response.capacity,
            prize: response.prize,
            available: response.available,
            pickups: response.pickups,
            deliveries: response.deliveries,
        })
    }

    pub async fn locate_station(
        &self,
        coords: Coordinates,
        num_stations: u32,
    ) -> Result<Vec<String>, Error> {
        match HubClient::new(self.channel.clone())
            .locate_station(Request::new(LocateStationRequest {
                num_stations,
                location: Some(GrpcCoordinates {
                    latitude: coords.latitude,
                    longitude: coords.longitude,
                }),
            }))
            .await
        {
            Ok(val) => Ok(val.into_inner().stations),
            Err(_) => todo!(),
        }
    }

    pub async fn bike_up(
        &self,
        user: &str,
        coords: Coordinates,
        station: &str,
    ) -> Result<(), Error> {
        match HubClient::new(self.channel.clone())
            .bike_up(Request::new(BikeUpRequest {
                user: user.to_owned(),
                location: Some(GrpcCoordinates {
                    latitude: coords.latitude,
                    longitude: coords.longitude,
                }),
                station: station.to_owned(),
            }))
            .await
        {
            Ok(_) => Ok(()),
            Err(e) => todo!("{}", e),
        }
    }

    pub async fn bike_down(
        &self,
        user: &str,
        coords: Coordinates,
        station: &str,
    ) -> Result<(), Error> {
        match HubClient::new(self.channel.clone())
            .bike_down(Request::new(BikeDownRequest {
                user: user.to_owned(),
                location: Some(GrpcCoordinates {
                    latitude: coords.latitude,
                    longitude: coords.longitude,
                }),
                station: station.to_owned(),
            }))
            .await
        {
            Ok(_) => Ok(()),
            Err(e) => todo!("{}", e),
        }
    }

    pub async fn ping(&self, message: String) -> Result<String, Error> {
        match HubClient::new(self.channel.clone())
            .ping(Request::new(PingRequest { message }))
            .await
        {
            Ok(val) => Ok(val.into_inner().message),
            Err(e) => todo!("{}", e),
        }
    }
}
