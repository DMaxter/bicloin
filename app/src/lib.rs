pub mod commands;
pub mod frontend;

#[derive(Clone)]
pub struct Coordinates {
    pub latitude: f64,
    pub longitude: f64,
}

impl Coordinates {
    pub fn new(latitude: f64, longitude: f64) -> Option<Self> {
        if Coordinates::check_latitude(latitude) && Coordinates::check_longitude(longitude) {
            Some(Coordinates {
                latitude,
                longitude,
            })
        } else {
            None
        }
    }

    fn check_latitude(lat: f64) -> bool {
        lat >= -90.0 && lat <= 90.0
    }

    fn check_longitude(lon: f64) -> bool {
        lon >= -180.0 && lon <= 180.0
    }
}

#[derive(PartialEq, Debug)]
pub enum Error {
    Coord, // Invalid coordinates
    Usage, // Wrong argument number
    GRPC, // FIXME: Correctly handle error, this is only temporary
    NotANumber, // Argument is not a number
    NoSuchLocation // Location doesn't exist
}
