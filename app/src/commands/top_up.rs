use super::Command;

use crate::frontend::Frontend;
use crate::Error;

pub struct TopUp<'a> {
    args: Vec<&'a str>,
    user: &'a str,
    phone: &'a str,
    frontend: &'a Frontend,
}

impl<'a> TopUp<'a> {
    pub fn new(args: Vec<&'a str>, user: &'a str, phone: &'a str, frontend: &'a Frontend) -> Self {
        TopUp {
            args,
            user,
            phone,
            frontend,
        }
    }
}

#[tonic::async_trait]
impl Command for TopUp<'_> {
    async fn execute(&mut self) -> Result<(), Error> {
        if self.args.len() != 1 {
            return Err(Error::Usage);
        }

        let amount = match self.args[0].parse::<u32>() {
            Ok(val) => val,
            Err(_) => return Err(Error::NotANumber),
        };

        let balance = match self.frontend.top_up(self.user, self.phone, amount).await {
            Ok(val) => val,
            Err(_) => return Err(Error::GRPC),
        };

        print!("   {} {} BIC\r", self.user, balance);

        Ok(())
    }
}
