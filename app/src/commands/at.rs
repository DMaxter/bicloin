use super::{Command, GOOGLE_URL};
use crate::{Coordinates, Error};

pub struct At<'a> {
    args: Vec<&'a str>,
    coords: &'a Coordinates,
    user: &'a str,
}

impl<'a> At<'a> {
    pub fn new(args: Vec<&'a str>, coords: &'a Coordinates, user: &'a str) -> Self {
        At { args, coords, user }
    }
}

#[tonic::async_trait]
impl Command for At<'_> {
    async fn execute(&mut self) -> Result<(), Error> {
        if self.args.len() != 0 {
            return Err(Error::Usage);
        }

        print!(
            "   {} em {}{},{}\r",
            self.user, GOOGLE_URL, self.coords.latitude, self.coords.longitude
        );

        Ok(())
    }
}
