use super::{Command, GOOGLE_URL};
use crate::{Coordinates, Error};

use std::collections::HashMap;

pub struct Move<'a> {
    args: Vec<&'a str>,
    old_coords: &'a mut Coordinates,
    user: &'a str,
    locations: &'a HashMap<String, Coordinates>,
}

impl<'a> Move<'a> {
    pub fn new(
        args: Vec<&'a str>,
        cur_coords: &'a mut Coordinates,
        user: &'a str,
        locations: &'a HashMap<String, Coordinates>,
    ) -> Self {
        Move {
            args,
            old_coords: cur_coords,
            user,
            locations,
        }
    }
}

#[tonic::async_trait]
impl Command for Move<'_> {
    async fn execute(&mut self) -> Result<(), Error> {
        let len = self.args.len();

        *self.old_coords = if len == 2 {
            match Coordinates::new(
                match self.args[0].parse::<f64>() {
                    Ok(l) => l,
                    Err(_) => return Err(Error::Coord),
                },
                match self.args[1].parse::<f64>() {
                    Ok(l) => l,
                    Err(_) => return Err(Error::Coord),
                },
            ) {
                Some(c) => c,
                None => return Err(Error::Coord),
            }
        } else if len == 1 {
            if !self.locations.contains_key(self.args[0]) {
                return Err(Error::NoSuchLocation);
            }

            self.locations.get(self.args[0]).unwrap().clone()
        } else {
            return Err(Error::Usage);
        };

        print!(
            "   {} em {}{},{}\r",
            self.user, GOOGLE_URL, self.old_coords.latitude, self.old_coords.longitude
        );

        Ok(())
    }
}
