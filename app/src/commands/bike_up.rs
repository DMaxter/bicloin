use super::Command;

use crate::frontend::Frontend;
use crate::{Coordinates, Error};

pub struct BikeUp<'a> {
    args: Vec<&'a str>,
    user: &'a str,
    coords: &'a Coordinates,
    frontend: &'a Frontend,
}

impl<'a> BikeUp<'a> {
    pub fn new(
        args: Vec<&'a str>,
        user: &'a str,
        coords: &'a Coordinates,
        frontend: &'a Frontend,
    ) -> Self {
        BikeUp {
            args,
            user,
            coords,
            frontend,
        }
    }
}

#[tonic::async_trait]
impl Command for BikeUp<'_> {
    async fn execute(&mut self) -> Result<(), Error> {
        if self.args.len() != 1 {
            return Err(Error::Usage);
        }

        let station = self.args[0];

        match self
            .frontend
            .bike_up(self.user, (*self.coords).clone(), station)
            .await
        {
            Ok(_) => Ok(()),
            Err(_) => todo!(),
        }
    }
}
