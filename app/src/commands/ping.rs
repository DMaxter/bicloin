use super::Command;
use crate::frontend::Frontend;
use crate::Error;

pub struct Ping<'a> {
    args: Vec<&'a str>,
    frontend: &'a Frontend,
}

impl<'a> Ping<'a> {
    pub fn new(args: Vec<&'a str>, frontend: &'a Frontend) -> Self {
        Ping { args, frontend }
    }
}

#[tonic::async_trait]
impl Command for Ping<'_> {
    async fn execute(&mut self) -> Result<(), Error> {
        match self
            .frontend
            .ping(if self.args.len() == 0 {
                "Bicloin System".to_owned()
            } else {
                self.args.join(" ")
            })
            .await
        {
            Ok(val) => print!("   {}\r", val),
            Err(_) => todo!(),
        };

        Ok(())
    }
}
