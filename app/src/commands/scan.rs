use super::Command;

use crate::frontend::Frontend;
use crate::{Coordinates, Error};

pub struct Scan<'a> {
    args: Vec<&'a str>,
    coords: &'a Coordinates,
    frontend: &'a Frontend,
}

impl<'a> Scan<'a> {
    pub fn new(args: Vec<&'a str>, coords: &'a Coordinates, frontend: &'a Frontend) -> Self {
        Scan {
            args,
            coords,
            frontend,
        }
    }
}

#[tonic::async_trait]
impl Command for Scan<'_> {
    async fn execute(&mut self) -> Result<(), Error> {
        if self.args.len() != 1 {
            return Err(Error::Usage);
        }

        let num = match self.args[0].parse::<u32>() {
            Ok(val) => val,
            Err(_) => return Err(Error::NotANumber),
        };

        let stations = match self
            .frontend
            .locate_station((*self.coords).clone(), num)
            .await
        {
            Ok(val) => val,
            Err(_) => todo!(),
        };

        for station in stations {
            match self.frontend.info_station(&station).await {
                Ok(val) => println!(
                    "{}, lat {}, {} long, {} docas, {} BIC prémio, {} bicicletas, a {} metros",
                    val.name,
                    val.location.latitude,
                    val.location.longitude,
                    val.capacity,
                    val.prize,
                    val.available,
                    haversine_distance(
                        self.coords.latitude.to_radians(),
                        self.coords.longitude.to_radians(),
                        val.location.latitude.to_radians(),
                        val.location.longitude.to_radians(),
                    )
                ),
                Err(_) => todo!(),
            }
        }

        Ok(())
    }
}

const EARTH_RADIUS: f64 = 6_367_444.5;

fn haversine_distance(lat1: f64, lon1: f64, lat2: f64, lon2: f64) -> u32 {
    (2. * EARTH_RADIUS
        * (((lat2 - lat1) / 2.).sin().powi(2)
            + lat1.cos() * lat2.cos() * ((lon2 - lon1) / 2.0).sin().powi(2))
        .sqrt()
        .asin()) as u32
}
