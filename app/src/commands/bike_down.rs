use super::Command;

use crate::frontend::Frontend;
use crate::{Coordinates, Error};

pub struct BikeDown<'a> {
    args: Vec<&'a str>,
    user: &'a str,
    coords: &'a Coordinates,
    frontend: &'a Frontend,
}

impl<'a> BikeDown<'a> {
    pub fn new(
        args: Vec<&'a str>,
        user: &'a str,
        coords: &'a Coordinates,
        frontend: &'a Frontend,
    ) -> Self {
        BikeDown {
            args,
            user,
            coords,
            frontend,
        }
    }
}

#[tonic::async_trait]
impl Command for BikeDown<'_> {
    async fn execute(&mut self) -> Result<(), Error> {
        if self.args.len() != 1 {
            return Err(Error::Usage);
        }

        let station = self.args[0];

        match self
            .frontend
            .bike_down(self.user, (*self.coords).clone(), station)
            .await
        {
            Ok(_) => Ok(()),
            Err(_) => todo!(),
        }
    }
}
