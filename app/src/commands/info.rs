use super::{Command, GOOGLE_URL};

use crate::frontend::Frontend;
use crate::Error;

pub struct Info<'a> {
    args: Vec<&'a str>,
    frontend: &'a Frontend,
}

impl<'a> Info<'a> {
    pub fn new(args: Vec<&'a str>, frontend: &'a Frontend) -> Self {
        Info { args, frontend }
    }
}

#[tonic::async_trait]
impl Command for Info<'_> {
    async fn execute(&mut self) -> Result<(), Error> {
        if self.args.len() != 1 {
            return Err(Error::Usage);
        }

        let station = self.args[0];

        let info = match self.frontend.info_station(station).await {
            Ok(val) => val,
            Err(_) => return Err(Error::GRPC),
        };

        let coords = info.location;

        print!("   {}, lat {}, {} long, {} docas, {} BIC prémio, {} bicicletas, {} levantamentos, {} devoluções, {}{},{}\r", info.name, coords.latitude, coords.longitude, info.capacity, info.prize, info.available, info.pickups, info.deliveries, GOOGLE_URL, coords.latitude, coords.longitude);

        Ok(())
    }
}
