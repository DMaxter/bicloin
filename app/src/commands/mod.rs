use crate::Error;

mod at;
mod balance;
mod bike_down;
mod bike_up;
mod info;
mod r#move;
mod ping;
mod scan;
mod tag;
mod top_up;

pub use self::at::*;
pub use self::balance::*;
pub use self::bike_down::*;
pub use self::bike_up::*;
pub use self::info::*;
pub use self::ping::*;
pub use self::r#move::*;
pub use self::scan::*;
pub use self::tag::*;
pub use self::top_up::*;

const GOOGLE_URL: &str = "https://www.google.com/maps/place/";

#[tonic::async_trait]
pub trait Command {
    async fn execute(&mut self) -> Result<(), Error>;
}
