use super::Command;
use crate::{Coordinates, Error};

use std::collections::HashMap;

pub struct Tag<'a> {
    args: Vec<&'a str>,
    locations: &'a mut HashMap<String, Coordinates>,
}

impl<'a> Tag<'a> {
    pub fn new(args: Vec<&'a str>, locations: &'a mut HashMap<String, Coordinates>) -> Self {
        Tag { args, locations }
    }
}

#[tonic::async_trait]
impl Command for Tag<'_> {
    async fn execute(&mut self) -> Result<(), Error> {
        if self.args.len() != 3 {
            return Err(Error::Usage);
        }

        self.locations.insert(
            self.args[2].to_owned(),
            match Coordinates::new(
                match self.args[0].parse::<f64>() {
                    Ok(l) => l,
                    Err(_) => return Err(Error::Coord),
                },
                match self.args[1].parse::<f64>() {
                    Ok(l) => l,
                    Err(_) => return Err(Error::Coord),
                },
            ) {
                Some(c) => c,
                None => return Err(Error::Coord),
            },
        );

        Ok(())
    }
}
