use super::Command;

use crate::frontend::Frontend;
use crate::Error;

pub struct Balance<'a> {
    args: Vec<&'a str>,
    user: &'a str,
    frontend: &'a Frontend,
}

impl<'a> Balance<'a> {
    pub fn new(args: Vec<&'a str>, user: &'a str, frontend: &'a Frontend) -> Self {
        Balance {
            args,
            user,
            frontend,
        }
    }
}

#[tonic::async_trait]
impl Command for Balance<'_> {
    async fn execute(&mut self) -> Result<(), Error> {
        if self.args.len() != 0 {
            return Err(Error::Usage);
        }

        let amount = match self.frontend.balance(self.user).await {
            Ok(val) => val,
            Err(_) => return Err(Error::GRPC),
        };
        print!("   {} {} BIC\r", self.user, amount);

        Ok(())
    }
}
